# Enunciado

Crear tres programas que permitan la simulación de estos tres
tipos de agentes.
- Tamaño de piso inicial aleatorio
- Suciedad de cada piso aleatoria (limpio, poco sucio, sucio, permanente)
- Posición inicial de la aspiradora aleatoria

Para poder visualizar y evaluar el comportamiento el programa
debe mostrar:
- El estado del piso para cada movimiento
- Las acciones que realiza la aspiradora
- La cantidad de movimientos totales ( mover, cambiar dirección, aspirar)
